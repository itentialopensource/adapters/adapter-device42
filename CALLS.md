## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Device42. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Device42.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Device42. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getBuildings(name, callback)</td>
    <td style="padding:15px">getBuildings</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/buildings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBuildings(body, callback)</td>
    <td style="padding:15px">postBuildings</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/buildings/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteBuildings(iD, callback)</td>
    <td style="padding:15px">deleteBuildings</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/buildings/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCustomFieldsBuilding(body, callback)</td>
    <td style="padding:15px">putCustom_fieldsBuilding</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/building/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRooms(name, buildingId, building, callback)</td>
    <td style="padding:15px">getRooms</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/rooms/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRooms(body, callback)</td>
    <td style="padding:15px">postRooms</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/rooms/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoomsID(iD, callback)</td>
    <td style="padding:15px">getRoomsID</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/rooms/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRoomsID(iD, body, callback)</td>
    <td style="padding:15px">putRoomsID</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/rooms/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRoomsID(iD, callback)</td>
    <td style="padding:15px">deleteRoomsID</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/rooms/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCustomFieldsRoom(body, callback)</td>
    <td style="padding:15px">putCustom_fieldsRoom</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/room/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceRoom(body, callback)</td>
    <td style="padding:15px">postDeviceRoom</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device/room/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceRoom(iD, callback)</td>
    <td style="padding:15px">deleteDeviceRoom</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device/room/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssetsRoom(body, callback)</td>
    <td style="padding:15px">postAssetsRoom</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/assets/room/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAssetsRoom(id, callback)</td>
    <td style="padding:15px">deleteAssetsRoom</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/assets/room/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRacks(name, buildingId, building, roomId, room, size, row, assetNo, manufacturer, callback)</td>
    <td style="padding:15px">getRacks</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/racks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRacks(body, callback)</td>
    <td style="padding:15px">postRacks</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/racks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRacksID(iD, callback)</td>
    <td style="padding:15px">getRacksID</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/racks/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRacksID(iD, callback)</td>
    <td style="padding:15px">deleteRacksID</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/racks/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCustomFieldsRack(body, callback)</td>
    <td style="padding:15px">putCustom_fieldsRack</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/rack/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssets(assetNo, serialNo, lastUpdatedLt, lastUpdatedGt, firstAddedLt, firstAddedGt, type, assetId, serviceLevel, customer, tags, tagsAnd, assetNoContains, customFieldsAnd, customFieldsOr, relatedDeviceId, callback)</td>
    <td style="padding:15px">getAssets</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/assets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssets(body, callback)</td>
    <td style="padding:15px">postAssets</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/assets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAssets(category, body, callback)</td>
    <td style="padding:15px">putAssets</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/assets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetsAssetId(assetId, includeCols, callback)</td>
    <td style="padding:15px">getAssetsAsset-id</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/assets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAssets(assetId, callback)</td>
    <td style="padding:15px">deleteAssets</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/assets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCustomFieldsAsset(body, callback)</td>
    <td style="padding:15px">putCustom_fieldsAsset</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/asset/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevices(type, deviceSubType, deviceSubTypeId, serviceLevel, inService, customer, tags, bladeHostName, virtualHostName, buildingId, building, roomId, room, rackId, rack, serialNo, serialNoContains, objectCategory, objectCategoryId, assetNo, name, tagsAnd, uuid, isItSwitch = 'yes', isItVirtualHost, isItBladeHost, hardware, hardwareIds, os, virtualSubtype, lastUpdatedLt, lastUpdatedGt, firstAddedLt, firstAddedGt, customFieldsAnd, customFieldsOr, callback)</td>
    <td style="padding:15px">getDevices</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesAll(includeCols, limit, offset, blankasnull, callback)</td>
    <td style="padding:15px">getDevicesAll</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/devices/all/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesId(deviceId, follow, callback)</td>
    <td style="padding:15px">getDevicesId</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/devices/id/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesCustomerId(customerId, includeCols, callback)</td>
    <td style="padding:15px">getDevicesCustomer_id</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/devices/customer_id/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesName(deviceName, includeCols, follow, callback)</td>
    <td style="padding:15px">getDevicesName</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/devices/name/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesSerial(deviceSerial, includeCols, callback)</td>
    <td style="padding:15px">getDevicesSerial</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/devices/serial/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesAsset(deviceAsset, includeCols, callback)</td>
    <td style="padding:15px">getDevicesAsset</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/devices/asset/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDevicesAttachments(deviceId, callback)</td>
    <td style="padding:15px">getAllDevicesAttachments</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/devices/id/{pathv1}/attachments/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadDevicesAttachments(deviceId, body, callback)</td>
    <td style="padding:15px">uploadDevicesAttachments</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/devices/id/{pathv1}/attachments/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getaDevicesAttachment(deviceId, attachmentId, callback)</td>
    <td style="padding:15px">getaDevicesAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/devices/id/{pathv1}/attachments/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDevicesAttachment(deviceId, id, callback)</td>
    <td style="padding:15px">deleteDevicesAttachment</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/devices/id/{pathv1}/attachments/{pathv2}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDevicesId(id, callback)</td>
    <td style="padding:15px">deleteDevicesId</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/devices/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDevice(body, callback)</td>
    <td style="padding:15px">postDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDevice(body, callback)</td>
    <td style="padding:15px">putDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMultiSerialDevice(body, callback)</td>
    <td style="padding:15px">postMultiSerialDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/multiserial_device/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMultiNodeDevice(body, callback)</td>
    <td style="padding:15px">postMultiNodeDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/multinodes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCloudInstanceDevice(body, callback)</td>
    <td style="padding:15px">postCloudInstanceDevice</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device/cloud_instance/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCustomField(body, callback)</td>
    <td style="padding:15px">putCustom_Field</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device/custom_field/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesImpactlist(deviceId, callback)</td>
    <td style="padding:15px">getDevicesImpactlist</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device/impactlist/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceMountpoints(deviceId, callback)</td>
    <td style="padding:15px">getDeviceMountpoints</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device/mountpoints/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceMountpoints(body, callback)</td>
    <td style="padding:15px">postDeviceMountpoints</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device/mountpoints/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceMountpoints(id, callback)</td>
    <td style="padding:15px">deleteDeviceMountpoints</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device/mountpoints/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceUrl(device, callback)</td>
    <td style="padding:15px">getDeviceUrl</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device/url/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceUrl(body, callback)</td>
    <td style="padding:15px">postDeviceUrl</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device/url/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDeviceUrl(body, callback)</td>
    <td style="padding:15px">putDeviceUrl</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device/url/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceUrl(id, callback)</td>
    <td style="padding:15px">deleteDeviceUrl</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device/url/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceRack(body, callback)</td>
    <td style="padding:15px">postDeviceRack</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device/rack/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceRackDeviceId(deviceId, callback)</td>
    <td style="padding:15px">deleteDeviceRackDevice_Id</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device/rack/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceIgnoreRules(callback)</td>
    <td style="padding:15px">getDevice_Ignore_Rules</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/device_ignore_rules/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">thiscallcreatesorupdatesadeviceignorerule(body, callback)</td>
    <td style="padding:15px">This call creates or updates a device ignore rule.</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/device_ignore_rules/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceIgnoreRules(id, callback)</td>
    <td style="padding:15px">deleteDevice_Ignore_Rules</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/device_ignore_rules/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getdeviceNameProfile(callback)</td>
    <td style="padding:15px">getdevice_name_profile</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device_name_profile/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">thiscallcreatesorupdatesadevicenameprofile(body, callback)</td>
    <td style="padding:15px">This call creates or updates a device name profile.</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device_name_profile/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletedeviceNameProfile(id, callback)</td>
    <td style="padding:15px">deletedevice_name_profile</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device_name_profile/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getObjectCategories(name, callback)</td>
    <td style="padding:15px">getObject_categories</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/object_categories/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postObjectCategories(body, callback)</td>
    <td style="padding:15px">postObject_categories</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/object_categories/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteObjectCategories(id, callback)</td>
    <td style="padding:15px">deleteObject_categories</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/object_categories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHardwares(name, type, deviceSubType, size, depth = 'half', partNo, watts, manufacturer, callback)</td>
    <td style="padding:15px">getHardwares</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/hardwares/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postHardwares(body, callback)</td>
    <td style="padding:15px">postHardwares</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/hardwares/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteHardwares(id, callback)</td>
    <td style="padding:15px">deleteHardwares</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/hardwares/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOperatingSystems(licensedCount, notLicensedCount, totalCount, category, callback)</td>
    <td style="padding:15px">getOperatingSystems</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/operatingsystems/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOperatingSystems(licensedCount, body, callback)</td>
    <td style="padding:15px">postOperatingSystems</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/operatingsystems/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOperatingSystems(id, callback)</td>
    <td style="padding:15px">deleteOperatingSystems</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/operatingsystems/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceOs(os, osId, callback)</td>
    <td style="padding:15px">getDevice_os</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device_os/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceOs(body, callback)</td>
    <td style="padding:15px">postDevice_os</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device_os/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceOs(deviceOsId, callback)</td>
    <td style="padding:15px">deleteDevice_os</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/device_os/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPduModels(name, pduModelId, callback)</td>
    <td style="padding:15px">getPdu_models</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/pdu_models/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPduModels(body, callback)</td>
    <td style="padding:15px">postPdu_models</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/pdu_models/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPduModelsPorts(body, callback)</td>
    <td style="padding:15px">postPdu_modelsPorts</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/pdu_models/ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPdus(name, type = 'pdu', pduModelId, pduModel, buildingId, roomId, rackId, serialNo, deviceId, callback)</td>
    <td style="padding:15px">getPdus</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/pdus/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPdus(body, callback)</td>
    <td style="padding:15px">postPdus</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/pdus/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPdus(body, callback)</td>
    <td style="padding:15px">putPdus</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/pdus/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPdusID(iD, callback)</td>
    <td style="padding:15px">getPdusID</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/pdus/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePdus(iD, callback)</td>
    <td style="padding:15px">deletePdus</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/pdus/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPdusRack(body, callback)</td>
    <td style="padding:15px">postPdusRack</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/pdus/rack/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePdusRack(id, callback)</td>
    <td style="padding:15px">deletePdusRack</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/pdus/rack/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPdusPorts(body, callback)</td>
    <td style="padding:15px">postPdusPorts</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/pdus/ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putPdusPorts(body, callback)</td>
    <td style="padding:15px">putPdusPorts</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/pdus/ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPatchPanelID(iD, callback)</td>
    <td style="padding:15px">getPatch_panelID</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/patch_panel/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPatchPanelPortsPatchPanelId(patchPanelId, callback)</td>
    <td style="padding:15px">getPatch_panel_portsPatch_panel_id</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/patch_panel_ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPatchPanelPortsPatchPanelId(backConnectionId, body, callback)</td>
    <td style="padding:15px">postPatch_panel_portsPatch_panel_id</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/patch_panel_ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPatchPanelModels(name, callback)</td>
    <td style="padding:15px">getPatch_panel_models</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/patch_panel_models/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPatchPanelModels(body, callback)</td>
    <td style="padding:15px">postPatch_panel_models</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/patch_panel_models/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPatchPanelModuleModels(callback)</td>
    <td style="padding:15px">getPatch_panel_module_models</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/patch_panel_module_models/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPatchPanelModuleModels(body, callback)</td>
    <td style="padding:15px">postPatch_panel_module_models</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/patch_panel_module_models/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getParts(type, device, deviceId, deviceSerial, room, roomId, partId, partmodelId, serialNo, lastUpdatedLt, lastUpdatedGt, rackId, rack, assetNo, customFieldsAnd, customFieldsOr, tags, callback)</td>
    <td style="padding:15px">getParts</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/parts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postParts(body, callback)</td>
    <td style="padding:15px">postParts</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/parts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteParts(id, callback)</td>
    <td style="padding:15px">deleteParts</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/parts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPartmodels(name, callback)</td>
    <td style="padding:15px">getPartmodels</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/partmodels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPartmodels(body, callback)</td>
    <td style="padding:15px">postPartmodels</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/partmodels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePartmodels(id, callback)</td>
    <td style="padding:15px">deletePartmodels</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/partmodels/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCustomFieldPart(body, callback)</td>
    <td style="padding:15px">putCustom_FieldPart</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/part/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCustomFieldPartmodel(id, body, callback)</td>
    <td style="padding:15px">putCustom_FieldPartmodel</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/partmodel/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPurchases(purchaseId, orderNo, vendor, costCenter, building, completed, callback)</td>
    <td style="padding:15px">getPurchases</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/purchases/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPurchases(body, callback)</td>
    <td style="padding:15px">postPurchases</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/purchases/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePurchases(id, callback)</td>
    <td style="padding:15px">deletePurchases</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/purchases/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCustomFieldPurchases(body, callback)</td>
    <td style="padding:15px">putCustom_FieldPurchases</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/purchases/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLifecycleEvent(type, device, asset, enduser, dateGt, dateLt, callback)</td>
    <td style="padding:15px">getLifecycle_event</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/lifecycle_event/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLifecycleEvent(body, callback)</td>
    <td style="padding:15px">putLifecycle_event</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/lifecycle_event/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMvrfgroup(callback)</td>
    <td style="padding:15px">getIPAMvrfgroup</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/vrfgroup/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIPAMvrfgroup(body, callback)</td>
    <td style="padding:15px">postIPAMvrfgroup</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/vrfgroup/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIPAMvrfgroup(body, callback)</td>
    <td style="padding:15px">putIPAMvrfgroup</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/vrfgroup/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPAMvrfgroupId(id, callback)</td>
    <td style="padding:15px">deleteIPAMvrfgroupId</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/vrfgroup/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIPAMCustomFIvrfgroup(body, callback)</td>
    <td style="padding:15px">putIPAMCustomFIvrfgroup</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/vrfgroup/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIPAMCustomFIsubnet(key, body, callback)</td>
    <td style="padding:15px">putIPAMCustomFIsubnet</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/subnet/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIPAMCustomFIipAddress(body, callback)</td>
    <td style="padding:15px">putIPAMCustomFIip_address</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/ip_address/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIPAMCustomFIswitchport(body, callback)</td>
    <td style="padding:15px">putIPAMCustomFIswitchport</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/switchport/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMsubnetcategory(callback)</td>
    <td style="padding:15px">getIPAMsubnetcategory</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/subnet_category/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIPAMsubnetcategory(body, callback)</td>
    <td style="padding:15px">postIPAMsubnetcategory</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/subnet_category/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIPAMsubnetcategory(body, callback)</td>
    <td style="padding:15px">putIPAMsubnetcategory</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/subnet_category/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPAMsubnetcategory(id, callback)</td>
    <td style="padding:15px">deleteIPAMsubnetcategory</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/subnet_category/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMsubnets(name, vrfGroupId, vrfGroup, parentSubnetId, parentSubnet, customerId, customer, subnetId, maskBits, maskBitsLt, maskBitsGt, description, rangeBegin, rangeEnd, gateway, tags, tagsAnd, customFieldsAnd, customFieldsOr, serviceLevel, category, categoryId, vlanId, network, callback)</td>
    <td style="padding:15px">getIPAMsubnets</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/subnets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIPAMsubnets(body, callback)</td>
    <td style="padding:15px">postIPAMsubnets</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/subnets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIPAMsubnets(body, callback)</td>
    <td style="padding:15px">putIPAMsubnets</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/subnets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMSubnetId(subnetId, callback)</td>
    <td style="padding:15px">getIPAM_subnet_id</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/subnets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPAMsubnets(subnetId, callback)</td>
    <td style="padding:15px">deleteIPAMsubnets</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/subnets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMCustomerId(customerId, callback)</td>
    <td style="padding:15px">getIPAM_customer_id</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/subnets/customer_id/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIPAMSubnetsCreateChild(body, callback)</td>
    <td style="padding:15px">postIPAM_subnets_create_child</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/subnets/create_child/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMvlans(vlanId, number, tags, tagsAnd, callback)</td>
    <td style="padding:15px">getIPAMvlans</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/vlans/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIPAMvlans(body, callback)</td>
    <td style="padding:15px">postIPAMvlans</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/vlans/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMvlansId(id, callback)</td>
    <td style="padding:15px">getIPAMvlans_id</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/vlans/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIPAMvlans(id, body, callback)</td>
    <td style="padding:15px">putIPAMvlans</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/vlans/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPAMvlans(id, callback)</td>
    <td style="padding:15px">deleteIPAMvlans</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/vlans/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIPAMvlansSmartMergeAll(body, callback)</td>
    <td style="padding:15px">postIPAMvlans_smart_merge_all</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/vlans/smart_merge_all/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMIps(offset, limit, subnetId, subnet, device, mac, available = 'yes', type = 'static', lastUpdatedLt, lastUpdatedGt, ip, firstAddedLt, firstAddedGt, ipId, label, tags, tagsAnd, customFieldsAnd, customFieldsOr, totalCount, ips, callback)</td>
    <td style="padding:15px">getIPAM_ips</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/ips/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIPAMIps(body, callback)</td>
    <td style="padding:15px">postIPAM_ips</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/ips/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPAMIps(id, callback)</td>
    <td style="padding:15px">deleteIPAM_ips</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/ips/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMIpsSubnetId(subnetId, callback)</td>
    <td style="padding:15px">getIPAM_ips_subnet_id</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/ips/subnet_id/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMSearch(query, string, callback)</td>
    <td style="padding:15px">getIPAM_search</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/search/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMSuggestSubnetId(id, maskBits, subnet, subnetId, vrfGroupId, vrfGroup, name, ifParentAssigned, ifParentAllocated, callback)</td>
    <td style="padding:15px">getIPAM_suggest_subnet_Id</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/suggest_subnet/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMSuggestIp(subnetId, subnet, name, vrfGroupId, vrfGroup, reserveIp = 'yes', number, maskBits, callback)</td>
    <td style="padding:15px">getIPAM_suggest_ip</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/suggest_ip/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMMacs(mac, device, deviceId, lastUpdatedLt, lastUpdatedGt, callback)</td>
    <td style="padding:15px">getIPAM_macs</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/macs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIPAMMacs(body, callback)</td>
    <td style="padding:15px">postIPAM_macs</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/macs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMMacsId(id, callback)</td>
    <td style="padding:15px">getIPAM_macs_id</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/macs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPAMMacsId(id, callback)</td>
    <td style="padding:15px">deleteIPAM_macs_id</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/macs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMSwitchports(switchId, switch2Id, lastUpdatedLt, lastUpdatedGt, firstAddedLt, firstAddedGt, tags, tagsAnd, includeCols, callback)</td>
    <td style="padding:15px">getIPAM_switchports</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/switchports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIPAMSwitchports(body, callback)</td>
    <td style="padding:15px">postIPAM_switchports</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/switchports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPAMSwitchports(id, callback)</td>
    <td style="padding:15px">deleteIPAM_switchports</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/switchports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMSwitchTemplates(callback)</td>
    <td style="padding:15px">getIPAM_switch_templates</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/switch_templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIPAMSwitches(body, callback)</td>
    <td style="padding:15px">postIPAM_switches</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/switches/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMTapPorts(id, callback)</td>
    <td style="padding:15px">getIPAM_tap_ports</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/tap_ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIPAMTapPorts(body, callback)</td>
    <td style="padding:15px">postIPAM_tap_ports</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/tap_ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMDnsRecords(domain, type, name, nameserver, content, tags, tagsAnd, dnsZone, ttl, changeDate, callback)</td>
    <td style="padding:15px">getIPAM_dns_records</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/dns/records/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIPAMDnsRecords(body, callback)</td>
    <td style="padding:15px">postIPAM_dns_records</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/dns/records/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPAMDnsRecords(id, callback)</td>
    <td style="padding:15px">deleteIPAM_dns_records</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/dns/records/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIPAMCustomFielddnsRecords(body, callback)</td>
    <td style="padding:15px">putIPAMCustom_Fielddns_records</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/dns_records/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMDnsZones(callback)</td>
    <td style="padding:15px">getIPAM__dns_zones</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/dns/zones/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIPAMDnsZones(body, callback)</td>
    <td style="padding:15px">postIPAM_dns_zones</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/dns/zones/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPAMDnsZones(id, callback)</td>
    <td style="padding:15px">deleteIPAM_dns_zones</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/dns/zones/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIPAMCustomFielddnsZone(body, callback)</td>
    <td style="padding:15px">putIPAMCustom_Fielddns_zone</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/dns_zone/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPAMIpnat(callback)</td>
    <td style="padding:15px">getIPAM_ipnat</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/ipnat/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIPAMIpnat(body, callback)</td>
    <td style="padding:15px">postIPAM_ipnat</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/ipnat/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIPAMIpnat(body, callback)</td>
    <td style="padding:15px">putIPAM_ipnat</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/ipnat/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIPAMIpnat(id, callback)</td>
    <td style="padding:15px">deleteIPAM_ipnat</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/ipnat/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppcomps(deviceId, device, callback)</td>
    <td style="padding:15px">getAppcomps</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/appcomps/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAppcomps(body, callback)</td>
    <td style="padding:15px">postAppcomps</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/appcomps/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppcompsAppcompId(appcompId, callback)</td>
    <td style="padding:15px">getAppcompsAppcomp_id</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/appcomps/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAppcomps(appcompId, callback)</td>
    <td style="padding:15px">deleteAppcomps</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/appcomps/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCustomFieldAppcomp(body, callback)</td>
    <td style="padding:15px">putCustom_FieldAppcomp</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/appcomp/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServices(serviceId, name, category, vendor, callback)</td>
    <td style="padding:15px">getServices</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/services/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServices(name, displayName, category, serviceType, notes, body, callback)</td>
    <td style="padding:15px">postServices</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/services/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServices(id, callback)</td>
    <td style="padding:15px">deleteServices</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/services/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceDetails(deviceId, device, serviceId, serviceDetailId, userId, callback)</td>
    <td style="padding:15px">getService_details</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/service_details/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServiceDetails(serviceName, serviceDisplayName, serviceType, category, startmode, state, device, appcomp, atLogon, atStartup, eventBased, idleTime, otherTrigger, otherType, minutes, hours, days, weeks, dayOfMonth, monthOfYear, dayOfWeek, body, callback)</td>
    <td style="padding:15px">postService_details</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/service_details/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceDetails(id, callback)</td>
    <td style="padding:15px">deleteService_details</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/service_detail/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicePorts(id, callback)</td>
    <td style="padding:15px">getService_ports</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/service_ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServicePorts(port, description, discoveredService, mappedService, body, callback)</td>
    <td style="padding:15px">postService_ports</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/service_ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServicePorts(id, callback)</td>
    <td style="padding:15px">deleteService_ports</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/service_ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getListenerConnectionStats(listenerDeviceName, port, callback)</td>
    <td style="padding:15px">getListener_connection_stats</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/listener_connection_stats/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkShares(callback)</td>
    <td style="padding:15px">getNetwork_shares</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/network_shares/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApi20ListenerConnectionStats(port, deviceName, listenerDeviceName, discoveredService, mappedService, serviceInstanceId, callback)</td>
    <td style="padding:15px">getListenerConnectionStats</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/listener_connection_stats/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getListenerConnectionStatsByID(id, callback)</td>
    <td style="padding:15px">getListenerConnectionStatsByID</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/listener_connection_stats/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduledTasks(id, deviceId, device, userId, callback)</td>
    <td style="padding:15px">getScheduledTasks</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/scheduled_tasks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postScheduledTasks(body, callback)</td>
    <td style="padding:15px">postScheduledTasks</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/scheduled_tasks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduledTasksByID(id, callback)</td>
    <td style="padding:15px">getScheduledTasksByID</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/scheduled_tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteScheduledTasksByID(id, callback)</td>
    <td style="padding:15px">deleteScheduledTasksByID</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/scheduled_tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServices2(id, displayname, category, vendor, callback)</td>
    <td style="padding:15px">getServices2</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/services/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServices2(body, callback)</td>
    <td style="padding:15px">postServices2</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/services/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServicesByID(id, callback)</td>
    <td style="padding:15px">deleteServicesByID</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceInstances(deviceId, device, serviceId, serviceDetailId, userId, callback)</td>
    <td style="padding:15px">getServiceInstances</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/service_instances/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServiceInstances(body, callback)</td>
    <td style="padding:15px">postServiceInstances</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/service_instances/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceInstancesByID(id, callback)</td>
    <td style="padding:15px">getServiceInstancesByID</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/service_instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceInstancesByID(id, callback)</td>
    <td style="padding:15px">deleteServiceInstancesByID</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/service_instances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceListenerPorts(port, deviceName, listenerDeviceName, discoveredService, mappedService, serviceInstanceId, callback)</td>
    <td style="padding:15px">getServiceListenerPorts</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/service_listener_ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServiceListenerPorts(body, callback)</td>
    <td style="padding:15px">postServiceListenerPorts</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/service_listener_ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceListenerPortsByID(id, callback)</td>
    <td style="padding:15px">getServiceListenerPortsByID</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/service_listener_ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putServiceListenerPorts(id, body, callback)</td>
    <td style="padding:15px">putServiceListenerPorts</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/service_listener_ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceClientConnections(id, callback)</td>
    <td style="padding:15px">getServiceClientConnections</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/service_client_connections/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIgnoredService(ignoredId, callback)</td>
    <td style="padding:15px">getIgnoredService</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/ignored_service/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIgnoredService(body, callback)</td>
    <td style="padding:15px">postIgnoredService</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/ignored_service/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIgnoredServiceByID(id, callback)</td>
    <td style="padding:15px">deleteIgnoredServiceByID</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/ignored_service/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSoftwareDetails(deviceId, device, softwareId, softwareDetailId, callback)</td>
    <td style="padding:15px">getSoftware_details</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/software_details/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateServicePorts(body, callback)</td>
    <td style="padding:15px">postUpdateService_ports</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/software_details/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSoftwareDetail(id, callback)</td>
    <td style="padding:15px">deleteSoftware_detail</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/software_details/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSoftwareComponentDetails(name, category, vendor, softwareType, tags, tagsAnd, callback)</td>
    <td style="padding:15px">getSoftware_Component_details</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/software/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSoftwareComponents(body, callback)</td>
    <td style="padding:15px">postUpdateSoftware_Components</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/software/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSoftwareComponent(id, callback)</td>
    <td style="padding:15px">deleteSoftware_Component</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/software/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSoftwareLicenseKeys(softwareId, softwareName, callback)</td>
    <td style="padding:15px">getSoftware_License_Keys</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/software/license_keys/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateSoftwareLicenses(body, callback)</td>
    <td style="padding:15px">postUpdateSoftware_Licenses</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/software/license_keys/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSoftwareLicenseKeys(id, callback)</td>
    <td style="padding:15px">deleteSoftware_License_Keys</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/software/license_keys/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificates(certificateId, callback)</td>
    <td style="padding:15px">get_certificates</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/certificates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCertificates(body, callback)</td>
    <td style="padding:15px">postUpdate_certificates</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/certificates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPassword(category, label, username, device, appcomp, id, plainText = 'yes', callback)</td>
    <td style="padding:15px">getPassword</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/passwords/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdatePasswords(body, callback)</td>
    <td style="padding:15px">post_Update_Passwords</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/passwords/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePassword(id, callback)</td>
    <td style="padding:15px">delete_Password</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/passwords/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomFields(body, callback)</td>
    <td style="padding:15px">post_Custom_Fields</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/password/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomers(includeCols, callback)</td>
    <td style="padding:15px">getCustomers</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/customers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomers(body, callback)</td>
    <td style="padding:15px">postCustomers</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/customers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomers(id, callback)</td>
    <td style="padding:15px">deleteCustomers</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/customers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCustomerContacts(body, callback)</td>
    <td style="padding:15px">post_Update_Customer_Contacts</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/customers/contacts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCustomFields(body, callback)</td>
    <td style="padding:15px">put_Custom Fields</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/customer/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendors(callback)</td>
    <td style="padding:15px">getVendors</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/vendors/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVendors(body, callback)</td>
    <td style="padding:15px">postVendors</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/vendors/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVendors(id, callback)</td>
    <td style="padding:15px">deleteVendors</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/vendors/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuits(iD, circuitId, type, orderDate, provisionDate, turnOnDate, notes, bandwidth, vendor, customer, dramcId, dmarc, originType = 'device', originId, originDevice, originCircuitId, originSwitchport, originSwitch, originPatchPanelPort, originPatchPanel, originPatchPanelId, originVendor, endPointType = 'device', endPointId, endPointDevice, endPointCircuitId, endPointSwitchport, endPointSwitch, endPointPatchPanelPort, endPointPatchPanel, endPointPatchPanelId, endPointVendor, callback)</td>
    <td style="padding:15px">getCircuits</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/circuits/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdateCircuits(body, callback)</td>
    <td style="padding:15px">post_Update_Circuits</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/circuits/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCircuit(id, callback)</td>
    <td style="padding:15px">deleteCircuit</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/circuits/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putApi10CustomFieldsCircuit(body, callback)</td>
    <td style="padding:15px">put_Custom_Fields</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/circuit/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPowerCircuits(breakerpanelId, bcpmId, callback)</td>
    <td style="padding:15px">get_All_Power_Circuits</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/powercircuits/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUpdatePowerCircuits(body, callback)</td>
    <td style="padding:15px">post_Update_PowerCircuits</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/powercircuits/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePowerCircuit(id, callback)</td>
    <td style="padding:15px">deletePowerCircuit</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/powercircuits/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCables(cableId, callback)</td>
    <td style="padding:15px">getCables</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/cables/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCables(body, callback)</td>
    <td style="padding:15px">postCables</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/cables/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCable(id, callback)</td>
    <td style="padding:15px">deleteCable</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/cables/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServiceLevel(callback)</td>
    <td style="padding:15px">getService_level</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/service_level/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServiceLevel(body, callback)</td>
    <td style="padding:15px">postService_level</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/service_level/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdmingroups(callback)</td>
    <td style="padding:15px">getAdmingroups</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/admingroups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdminusers(callback)</td>
    <td style="padding:15px">getAdminusers</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/adminusers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAdminUsers(body, callback)</td>
    <td style="padding:15px">postAdminUsers</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/adminusers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEndusers(callback)</td>
    <td style="padding:15px">getEndusers</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/endusers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEndUsers(body, callback)</td>
    <td style="padding:15px">postEndUsers</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/endusers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEndUsers(id, callback)</td>
    <td style="padding:15px">deleteEndUsers</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/endusers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReports(id, csvFormat, callback)</td>
    <td style="padding:15px">getReports</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/reports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHistory(callback)</td>
    <td style="padding:15px">getHistory</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/history/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHistoryNumberOfWeeks(numberOfWeeks, callback)</td>
    <td style="padding:15px">getHistoryNumber_of_weeks</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/history/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditlogs(objectId, contentType, actionTimeGt, actionTimeLt, callback)</td>
    <td style="padding:15px">getAuditlogs</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auditlogs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutoDiscoveryNetworks(callback)</td>
    <td style="padding:15px">getAuto_discoveryNetworks</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/networks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAutoDiscoveryNetworks(body, callback)</td>
    <td style="padding:15px">postAuto_discoveryNetworks</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/networks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAutoDiscoveryNetworks(body, callback)</td>
    <td style="padding:15px">putAuto_discoveryNetworks</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/networks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutoDiscoverynmap(callback)</td>
    <td style="padding:15px">getAuto_discoverynmap</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/nmap/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAutoDiscoverynmap(body, callback)</td>
    <td style="padding:15px">postAuto_discoverynmap</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/nmap/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAutoDiscoverynmap(body, callback)</td>
    <td style="padding:15px">putAuto_discoverynmap</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/nmap/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutoDiscoveryPingsweep(callback)</td>
    <td style="padding:15px">getAuto_discoveryPingsweep</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/pingsweep/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAutoDiscoveryPingsweep(body, callback)</td>
    <td style="padding:15px">postAuto_discoveryPingsweep</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/pingsweep/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAutoDiscoveryPingsweep(body, callback)</td>
    <td style="padding:15px">putAuto_discoveryPingsweep</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/pingsweep/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAutoDiscoveryBladeDisc(body, callback)</td>
    <td style="padding:15px">postAuto_discoveryBlade_disc</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/blade_disc/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAutoDiscoveryBladeDisc(body, callback)</td>
    <td style="padding:15px">putAuto_discoveryBlade_disc</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/blade_disc/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAutoDiscoveryPowerDisc(body, callback)</td>
    <td style="padding:15px">postAuto_discoveryPower_disc</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/power_disc/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAutoDiscoveryPowerDisc(body, callback)</td>
    <td style="padding:15px">putAuto_discoveryPower_disc</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/power_disc/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutoDiscoveryvServer(callback)</td>
    <td style="padding:15px">getAuto_discoveryvServer</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/vserver/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAutoDiscoveryVserver(body, callback)</td>
    <td style="padding:15px">postAuto_discoveryVserver</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/vserver/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAutoDiscoveryVserver(body, callback)</td>
    <td style="padding:15px">putAuto_discoveryVserver</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/vserver/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutoDiscoveryCloudaccount(callback)</td>
    <td style="padding:15px">getAuto_discoveryCloudaccount</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/cloudaccount/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAutoDiscoveryCloudaccount(body, callback)</td>
    <td style="padding:15px">postAuto_discoveryCloudaccount</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/cloudaccount/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAutoDiscoveryCloudaccount(body, callback)</td>
    <td style="padding:15px">putAuto_discoveryCloudaccount</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/cloudaccount/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudInfrastructure(callback)</td>
    <td style="padding:15px">getCloud_infrastructure</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/cloud_infrastructure/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCloudInfrastructure(body, callback)</td>
    <td style="padding:15px">postCloud_infrastructure</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/cloud_infrastructure/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudInfrastructure(id, callback)</td>
    <td style="padding:15px">deleteCloud_infrastructure</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/cloud_infrastructure/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCustomFieldCloudInfrastructure(body, callback)</td>
    <td style="padding:15px">putCustom_FieldCloud_infrastructure</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/cloud_infrastructure/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudResource(callback)</td>
    <td style="padding:15px">getCloud_resource</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/resource/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCloudResource(body, callback)</td>
    <td style="padding:15px">postCloud_resource</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/resource/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCloudResource(id, callback)</td>
    <td style="padding:15px">deleteCloud_resource</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/resource/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCustomFieldCloudResource(body, callback)</td>
    <td style="padding:15px">putCustom_FieldCloud_resource</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/custom_fields/resource/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourceRelationship(callback)</td>
    <td style="padding:15px">getResource_relationship</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/resource_relationship/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResourceRelationship(id, callback)</td>
    <td style="padding:15px">deleteResource_relationship</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/resource_relationship/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutoDiscoverySnmpDisc(callback)</td>
    <td style="padding:15px">getAuto_discoverySnmp_disc</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/snmp_disc/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAutoDiscoverySnmpDisc(body, callback)</td>
    <td style="padding:15px">postAuto_discoverySnmp_disc</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/snmp_disc/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAutoDiscoverySnmpDisc(body, callback)</td>
    <td style="padding:15px">putAuto_discoverySnmp_disc</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/snmp_disc/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutoDiscoveryIpmi(callback)</td>
    <td style="padding:15px">getAuto_discoveryIpmi</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/ipmi/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAutoDiscoveryIpmi(hostnameToUse = '1 (Serial # / Reverse DNS / IP)', body, callback)</td>
    <td style="padding:15px">postAuto_discoveryIpmi</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/ipmi/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAutoDiscoveryIpmi(body, callback)</td>
    <td style="padding:15px">putAuto_discoveryIpmi</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/ipmi/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutoDiscoveryUcs(callback)</td>
    <td style="padding:15px">getAuto_discoveryUcs</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/ucs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAutoDiscoveryUcs(body, callback)</td>
    <td style="padding:15px">postAuto_discoveryUcs</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/ucs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAutoDiscoveryUcs(body, callback)</td>
    <td style="padding:15px">putAuto_discoveryUcs</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/ucs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutoDiscoveryDns(callback)</td>
    <td style="padding:15px">getAuto_discoveryDns</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/dns/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAutoDiscoveryDns(body, callback)</td>
    <td style="padding:15px">postAuto_discoveryDns</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/dns/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAutoDiscoveryDns(body, callback)</td>
    <td style="padding:15px">putAuto_discoveryDns</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/dns/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAutoDiscoveryCertificate(callback)</td>
    <td style="padding:15px">getAuto_discoveryCertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/certificate/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAutoDiscoveryCertificate(body, callback)</td>
    <td style="padding:15px">postAuto_discoveryCertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/certificate/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAutoDiscoveryCertificate(body, callback)</td>
    <td style="padding:15px">putAuto_discoveryCertificate</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/auto_discovery/certificate/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHealthstats(callback)</td>
    <td style="padding:15px">getHealthstats</td>
    <td style="padding:15px">{base_path}/{version}/:4343/healthstats/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBackupSchedules(body, callback)</td>
    <td style="padding:15px">postBackup_schedules</td>
    <td style="padding:15px">{base_path}/{version}/:4343/api/1.0/backup_schedules/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBackupSchedules(callback)</td>
    <td style="padding:15px">getBackup_schedules</td>
    <td style="padding:15px">{base_path}/{version}/:4343/api/1.0/backup_schedules/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAppliancemode(body, callback)</td>
    <td style="padding:15px">postAppliancemode</td>
    <td style="padding:15px">{base_path}/{version}/:4343/api/1.0/appliancemode/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTags(callback)</td>
    <td style="padding:15px">getTags</td>
    <td style="padding:15px">{base_path}/{version}/api/1.0/tags/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
