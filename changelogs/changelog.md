
## 0.2.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-device42!1

---

## 0.1.1 [03-19-2021]

- Initial Commit

See commit 8169779

---
